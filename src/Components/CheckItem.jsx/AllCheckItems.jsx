import React from "react";

const key = "f1fc58945d17dbaac59bedb8364274cf";
const token =
  "ATTA6efd8ca5a96337966c66ba73c0c2a54b4a1c42008d236f8e9fff33bfdcd8034638704FC5";

import { useState, useEffect } from "react";
import axios from "axios";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

import DeleteIcon from "@mui/icons-material/Delete";

import CreateCheckItem from "./CreateCheckItem";
import { FormControlLabel, Checkbox } from "@mui/material";
import LinearProgress from '@mui/material/LinearProgress';

const AllCheckItems = ({ id, cardId }) => {
  const [checkItems, setCheckItems] = useState(null);

  const [Progress, setProgress] = useState(0);

  const updateCheckItems = (newData) => {
    const updatedItems = [...checkItems, newData];
    setCheckItems(updatedItems);
    CalculateProgress(updatedItems);
  };

  const CalculateProgress = (values) => {
    if(values.length>0){
    const completedCount = values.filter((item) => item.checked).length;
    const progressValue = Math.round((completedCount / values.length) * 100);
    setProgress(progressValue);
    }
    else{
      setProgress(0)
    }
  };

  useEffect(() => {
    const url = `https://api.trello.com/1/checklists/${id}/checkItems?key=${key}&token=${token}`;

    axios
      .get(url)
      .then((response) => {
        setCheckItems(response.data);
        CalculateProgress(response.data);
      })
      .catch((error) => {
        console.error("Error fetching boards", error);
      });
  }, [id]);

  const deleteEachItem = (itemid) => {
    const url = `https://api.trello.com/1/checklists/${id}/checkItems/${itemid}?key=${key}&token=${token}`;
    axios
      .delete(url)
      .then(() => {
        handleDeleteCheckItem(itemid);
      })
      .catch((error) => {
        console.error("Error deleting cards", error);
      });
  };

  const handleDeleteCheckItem = (itemid) => {
    const updatedItems = checkItems.filter((item) => item.id !== itemid);
    setCheckItems(updatedItems);
    CalculateProgress(updatedItems);
  };


  const handleCheck = (itemId)=> async(event) => {
    
    const newState = event.target.checked;
    try {
      const url = `https://api.trello.com/1/cards/${cardId}/checkItem/${itemId}?key=${key}&token=${token}&state=${newState}`;
      await axios.put(url);
      const updatedCheckItems = checkItems.map((item) =>
        item.id === itemId ? { ...item, checked: newState } : item
      );
      setCheckItems(updatedCheckItems);
      CalculateProgress(updatedCheckItems);
    } catch (error) {
      console.error("Error updating check item", error);
    }
  };



  return (
    <div>
      {checkItems ? (

        <div className="flex flex-col">
          <p>{`${Progress}%`}</p>
          <LinearProgress style={{marginTop:"20px",marginBottom:"20px"}}variant="determinate" value={Progress} />
          {checkItems.map((item) => (
            <Card key={item.id} style={{ backgroundColor:"lightgray",margin: "10px 0px 0px 0px" }}>
              <CardContent>
                {/* <Typography>{item.name}</Typography> */}
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={item.checked || false}
                      onChange={handleCheck(item.id)}
                    />
                  }
                  label={<Typography>{item.name}</Typography>}
                />
                <DeleteIcon
                  style={{
                    cursor: "pointer",
                    float: "right",
                    
                  }}
                  onClick={(e) => {
                    e.stopPropagation();
                    deleteEachItem(item.id);
                  }}
                />
              </CardContent>
              
            </Card>
            
          ))}
          
          
        </div>
      ) : (
        <p>Loading...</p>
      )}

      <CreateCheckItem id={id} updateCheckItems={updateCheckItems} />
    </div>
  );
};

export default AllCheckItems;
