
import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { Card, CardContent, Dialog, DialogTitle, DialogContent, DialogActions } from "@mui/material";
import axios from "axios";

const key = "f1fc58945d17dbaac59bedb8364274cf";
const token = "ATTA6efd8ca5a96337966c66ba73c0c2a54b4a1c42008d236f8e9fff33bfdcd8034638704FC5";

const CreateCheckItem= (props) => {
    const {id,updateCheckItems}=props
 
  const [checkItemName, setCheckItemName] = useState("");
  const [showInputField, setShowInputField] = useState(false);

  const handleCheckItemNameChange = (e) => {
    setCheckItemName(e.target.value);
  };

  const handleAddCardClick = () => {
    setShowInputField(true);
  };

  const handleCreateCheckItem = async () => {
    const API_POST_URL = `https://api.trello.com/1/checklists/${id}/checkItems?name=${checkItemName}&key=${key}&token=${token}`;
    if (checkItemName.trim()) {
      try {
        const response = await axios.post(API_POST_URL);
       
        updateCheckItems(response.data);
        setShowInputField(false);
        setCheckItemName(""); 
      } catch (error) {
        console.error("Error creating board:", error);
     
      }
    }
  };

  const handleClose = () => {
    setShowInputField(false);
    setCheckItemName(""); 
  };

  return (
    <div>
      <Dialog open={showInputField} onClose={handleClose} maxWidth="xs" fullWidth>
        <DialogTitle>Create item</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="outlined-basic"
            label="Item Name"
            variant="outlined"
            fullWidth
            value={checkItemName}
            onChange={handleCheckItemNameChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleCreateCheckItem} color="primary">
            Create
          </Button>
        </DialogActions>
      </Dialog>

      {!showInputField && (
        <Card style={{alignContent:"center" ,margin:"10px 0px 0px 0px"}}>
        <CardContent>
        <Button variant="outlined" style={{backgroundColor:"lightgray",padding:"2px 2px",border:"none", color:"black", }}onClick={handleAddCardClick} >
          Add item
        </Button>
        </CardContent>
        </Card>
      )}
    </div>
  );
};

export default CreateCheckItem;

