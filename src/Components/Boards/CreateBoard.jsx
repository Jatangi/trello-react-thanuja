
import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { Card, CardContent, Dialog, DialogTitle, DialogContent, DialogActions } from "@mui/material";
import axios from "axios";

const key = "f1fc58945d17dbaac59bedb8364274cf";
const token = "ATTA6efd8ca5a96337966c66ba73c0c2a54b4a1c42008d236f8e9fff33bfdcd8034638704FC5";

const CreateBoard = ({ updateBoards }) => {
  const [boardName, setBoardName] = useState("");
  const [showInputField, setShowInputField] = useState(false);

  const handleBoardNameChange = (e) => {
    setBoardName(e.target.value);
  };

  const handleAddBoardClick = () => {
    setShowInputField(true);
  };

  const handleCreateBoard = async () => {
    const API_POST_URL = `https://api.trello.com/1/boards/?name=${boardName}&key=${key}&token=${token}`;
    if (boardName.trim()) {
      try {
        const response = await axios.post(API_POST_URL);
      
        updateBoards(response.data);
        setShowInputField(false);
        setBoardName(""); 
      } catch (error) {
        console.error("Error creating board:", error);
      }
    }
  };

  const handleClose = () => {
    setShowInputField(false);
    setBoardName(""); 
  };

  return (
    <div>
      <Dialog open={showInputField} onClose={handleClose} maxWidth="xs" fullWidth>
        <DialogTitle>Create New Board</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="outlined-basic"
            label="Board Name"
            variant="outlined"
            fullWidth
            value={boardName}
            onChange={handleBoardNameChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleCreateBoard} color="primary">
            Create
          </Button>
        </DialogActions>
      </Dialog>

      {!showInputField && (
        <Card style={{ backgroundColor: "#BFDBFE", width: "500px", }}>
          <CardContent>
            <Button variant="outlined" style={{ width: "250px", border: "none", color: "black" }} onClick={handleAddBoardClick}>
              Add Board
            </Button>
          </CardContent>
        </Card>
      )}
    </div>
  );
};

export default CreateBoard;
