import React from "react";

// import { getAllBoards } from "../../API-helpers/Board-api-helper";

// import { useState, useEffect } from "react";

// import Card from "@mui/material/Card";

// import CardContent from "@mui/material/CardContent";

// import axios from "axios";

// import Typography from "@mui/material/Typography";


// const Allboards = () => {

//   const [boards, setBoards] = useState(null);

//   // useEffect(() => {
//   //   const getAllBoards = async () => {
//   //     const res = await axios
//   //       .get(
//   //         `https://api.trello.com/1/members/me/boards?key=c7db9cc0857854022051b6a31fa55037&token=ATTAbb8a2d04151de54a6e47abd62c59ef674b4f60ca5cdf276f1c98503ae5755c9d87D544CA`
//   //       )
//   //       const data = await res.data;
//   //       console.log(data);
//   //       setBoards(data)
//   //      .catch((err) => console.log(err)); 
//   //   };
//   //   getAllBoards()
//   // }, []);
//   // console.log(boards)
//   useEffect(()=>{
//     const url =`https://api.trello.com/1/members/me/boards?key=c7db9cc0857854022051b6a31fa55037&token=ATTAbb8a2d04151de54a6e47abd62c59ef674b4f60ca5cdf276f1c98503ae5755c9d87D544CA`
//     axios.get(url)
//     .then((response)=>{
//       console.log(response.data);
//       // setBoards(data);
//     })
//     .catch((error)=>{
//       console.error('Error fetching boards',error);
//     })
//   },[]);

//   return (
//     <div>
//       <h2>All Boards</h2>
//       {boards ? (       
//         <div className="grid grid-cols-3 gap-4 m-40 ">
//           {boards.map((board) => (
//             <Card key={board.id}>
//               <CardContent>
//                 <Typography>
//                   {board.name}
//                 </Typography>
//               </CardContent>
//             </Card>
//           ))}
//         </div>
//       ) : (
//         <p>Loading...</p>
//       )}
//     </div>
//   );
// };

// export default Allboards;



import { useState, useEffect } from 'react';
import axios from 'axios';
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

import CreateBoard from "./CreateBoard";
import { useNavigate } from "react-router-dom";


const Allboards = () => {
  const [boards, setBoards] = useState(null);
  const navigate=useNavigate()
  
  const updateBoards=(newData)=>{
    setBoards([...boards,newData]);
  }

  const EachBoard=(id)=>{
    navigate(`/boards/${id}`)
  }
  useEffect(() => {
    const url = `https://api.trello.com/1/members/me/boards?key=c7db9cc0857854022051b6a31fa55037&token=ATTAbb8a2d04151de54a6e47abd62c59ef674b4f60ca5cdf276f1c98503ae5755c9d87D544CA`;
    
    axios.get(url)
      .then((response) => {
      
        setBoards(response.data);
      })
      .catch((error) => {
        console.error('Error fetching boards', error);
      });
  }, []);

  return (
    <div>
   
      {boards ? (
        <div className="grid grid-cols-3 gap-4 m-40 ">
          {boards.map((board) => (
            <Card key={board.id} style={{backgroundColor: "#BFDBFE"}} onClick={()=>{EachBoard(board.id)}}>
              <CardContent>
                <Typography>
                  {board.name}
                </Typography>
              </CardContent>
            </Card>
          ))}
            <CreateBoard updateBoards={updateBoards}/>
        </div>
      ) : (
        <p>Loading...</p>
      )}

    
    </div>
  );
};

export default Allboards;