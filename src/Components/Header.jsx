import React from 'react'
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import { useNavigate } from 'react-router-dom';

export const Header = () => {
  const navigate=useNavigate()
  return (
    <div>
        <div className='border-2 h-20 bg-blue-200'>
            <p className='text-xl mt-5 ml-5'  onClick={() => navigate(-1)}>Trello</p>
            <AccountCircleIcon style={{float:"right" ,fontSize:"50px",marginTop:"-30px"}}/>
        </div>
    </div>
  )
}
