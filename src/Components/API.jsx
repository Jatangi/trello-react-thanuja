import axios from 'axios';

const key = "f1fc58945d17dbaac59bedb8364274cf";
const token = "ATTA6efd8ca5a96337966c66ba73c0c2a54b4a1c42008d236f8e9fff33bfdcd8034638704FC5";

const api = axios.create({
  baseURL: 'https://api.trello.com/1/',
  params: {
    key: key,
    token: token
  }
});

export default api;
