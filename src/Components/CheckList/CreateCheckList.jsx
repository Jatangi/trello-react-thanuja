
import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { Card, CardContent, Dialog, DialogTitle, DialogContent, DialogActions } from "@mui/material";
import axios from "axios";
import { resolvePath } from "react-router-dom";

const key = "f1fc58945d17dbaac59bedb8364274cf";
const token = "ATTA6efd8ca5a96337966c66ba73c0c2a54b4a1c42008d236f8e9fff33bfdcd8034638704FC5";

const CreateCheckList = (props) => {
    const {id, updateCheckList}=props
    
 
  const [CheckListName, setCheckListName] = useState("");
  const [showInputField, setShowInputField] = useState(false);

  const handleCheckListNameChange = (e) => {
    setCheckListName(e.target.value);
  };

 

  const handleCreateCheckList = async () => {
   
   
    const API_POST_URL = `https://api.trello.com/1/cards/${id}/checklists?name=${CheckListName}&key=${key}&token=${token}`;
    if (CheckListName!=="") {
      try {
        const response = await axios.post(API_POST_URL);
        console.log("............")
        //updateCheckList(response.data);
        updateCheckList(response.data)
       
        setShowInputField(false);
       
        setCheckListName(""); 
      } catch (error) {
        console.error("Error creating board:", error);
     
      }
    }
  };

  const handleClose = () => {
    setShowInputField(false);
    setCheckListName(""); 
  };
  const handleAddCheckListClick=()=>{
    setShowInputField(true)
  }
  

  return (
    <div>
      {!showInputField && (
        <Card style={{ backgroundColor:"lightgray",align:"center" ,float:"right" ,marginTop:"-450px" ,height:"50px" ,}}>
        <CardContent>
        <Button variant="outlined" style={{border:"none", color:"black",}} onClick={handleAddCheckListClick} >
          Add CheckList
        </Button>
        </CardContent>
        </Card>
      )}
     
      <Dialog open={showInputField} onClose={handleClose} maxWidth="xs" fullWidth>
        <DialogTitle>Create New CheckList</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="outlined-basic"
            label="Check List Name"
            variant="outlined"
            fullWidth
            value={CheckListName}
            onChange={handleCheckListNameChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleCreateCheckList} color="primary">
            Create
          </Button>
        </DialogActions>
        
      </Dialog>
      
    </div>
  );
};

export default CreateCheckList;

