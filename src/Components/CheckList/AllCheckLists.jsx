

const key = "f1fc58945d17dbaac59bedb8364274cf";
const token =
  "ATTA6efd8ca5a96337966c66ba73c0c2a54b4a1c42008d236f8e9fff33bfdcd8034638704FC5";


import React, { useState, useEffect } from "react";
import DeleteIcon from '@mui/icons-material/Delete';
import axios from "axios";
import {
  Card,
  CardContent,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  TextField,
} from "@mui/material";
import CreateCheckList from "./CreateCheckList";
import AllCheckItems from "../CheckItem.jsx/AllCheckItems";


const AllCheckLists = ({ id }) => {
  const [checkLists, setCheckLists] = useState([]);
  const [showInputField, setShowInputField] = useState(true);

  const handleAddCardClick = () => {
    setShowInputField(true);
  };

  const updateCheckList=(newData)=>{
    setCheckLists([...checkLists,newData])
  }

  const handleClose = () => {
    setShowInputField(false);
  };

  useEffect(() => {
    const url = `https://api.trello.com/1/cards/${id}/checklists?key=${key}&token=${token}`;

    axios
      .get(url)
      .then((response) => {
        setCheckLists(response.data);
      })
      .catch((error) => {
        console.error("Error fetching checklists", error);
      });
  }, [id]); 

  const  DeleteCheckList=(id)=>{
    //https://api.trello.com/1/checklists/{id}?key=APIKey&token=APIToken
    const url=`https://api.trello.com/1/checklists/${id}?key=${key}&token=${token}`
   axios.delete(url)
   .then(()=>{
    handleDeleteCheckList(id);
   })
   .catch((error)=>{
    console.error('Error deleting cards',error);
   })
  }

  const handleDeleteCheckList=(id)=>{
    setCheckLists(checkLists.filter(each=>each.id!==id))
  }
  return (
    <div>
      <Dialog open={showInputField} onClose={handleClose} maxWidth="lg" fullWidth>
        <DialogTitle></DialogTitle>
        <DialogContent>
          
          {checkLists ? (
        <div className="flex flex-col">
          {checkLists.map((checklist) => (
            <Card
              key={checklist.id}
              style={{
               
                margin: "10px 0px 0px 30px",
                width: "50%",
              }}
            >
              <CardContent>
                <Typography>{checklist.name}</Typography>
              </CardContent>
              <DeleteIcon style={{ cursor: 'pointer' ,float:"right" ,marginTop:"-20px"}} onClick={(e) =>{e.stopPropagation();
                  DeleteCheckList(checklist.id) 
                   }}
                   />
              <AllCheckItems id={checklist.id} cardId={id}/>
              <Card style={{backgroundColor:"lightgray"}}></Card>  
            </Card>
            
            
              
          ))}
          <CreateCheckList id={id} updateCheckList={updateCheckList}/>
        </div>
      ) : (
        <p>Loading checklists...</p>
      )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>

    </div>
  );
};

export default AllCheckLists;



