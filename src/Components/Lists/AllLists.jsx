import React from "react";
import { useState, useEffect } from "react";
import axios from "axios";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

import { useNavigate, useParams } from "react-router-dom";
import CreateList from "./CreateList";
import { Button } from "@mui/material";
import AllCards from "../Cards/AllCards";
import DeleteIcon from '@mui/icons-material/Delete';

const key = "f1fc58945d17dbaac59bedb8364274cf";
const token =
  "ATTA6efd8ca5a96337966c66ba73c0c2a54b4a1c42008d236f8e9fff33bfdcd8034638704FC5";

const AllLists = (props) => {
  const { id } = useParams();

  const [Lists, setLists] = useState(null);
  const navigate = useNavigate();

  const updateLists = (newData) => {
    setLists([...Lists, newData]);
  };

  useEffect(() => {
    const url = `https://api.trello.com/1/boards/${id}/lists?key=${key}&token=${token}`;

    axios
      .get(url)
      .then((response) => {
    
        setLists(response.data);
      })
      .catch((error) => {
        console.error("Error fetching boards", error);
      });
  }, []);

  const deleteEachList=(id)=>{
    const url=` https://api.trello.com/1/lists/${id}/closed?key=${key}&token=${token}&value=true`
    axios.put(url)
    .then(()=>{
    handleDeleteList(id);
    })
    .catch((error)=>{
     console.error('Error deleting cards',error);
    })
 }
 
   const handleDeleteList=(id)=>{
     setLists(Lists.filter(list=>list.id!==id))
   }
 

  return (
    <div>
    
      {Lists && Lists.length > 0 ? (
        <div className="grid grid-cols-3 gap-4 m-40 ">
          {Lists.map((list) => (
            <Card key={list.id} style={{ backgroundColor: "#BFDBFE" }}>
              <CardContent>
              <DeleteIcon style={{ cursor: 'pointer' ,float:"right" ,marginTop:"-10px"}} onClick={(e) =>{e.stopPropagation();
                   deleteEachList(list.id)
                   }}
                   />
                <Typography>{list.name}</Typography>

                <AllCards id={list.id} />
              </CardContent>
             
            </Card>
          ))}
           <CreateList updateLists={updateLists} id={id} />
        </div>
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
};

export default AllLists;
