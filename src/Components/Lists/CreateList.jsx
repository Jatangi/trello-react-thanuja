import React, { useState, useEffect } from "react";
import { getBoard } from "../../API-helpers/Board-api-helper";
import { useParams } from "react-router-dom";
import { Card, CardContent, Dialog, DialogTitle, DialogContent, DialogActions,TextField,Button } from "@mui/material";
import axios from "axios";

const key = "f1fc58945d17dbaac59bedb8364274cf";
const token = "ATTA6efd8ca5a96337966c66ba73c0c2a54b4a1c42008d236f8e9fff33bfdcd8034638704FC5";


const CreateList = ({updateLists}) => {
  const {id}=useParams()
  const [listName,setListName]=useState("")
  const [showInputField, setShowInputField] = useState(false);


 
  const handleListNameChange = (e) => {
    setListName(e.target.value);
  };

  const handleAddListClick = () => {
    setShowInputField(true);
  };

  const handleCreateList = async () => {
    const API_POST_URL = `https://api.trello.com/1/lists?name=${listName}&idBoard=${id}&key=${key}&token=${token}`
    if (listName.trim()) {
      try {
        const response = await axios.post(API_POST_URL);
     
        updateLists(response.data);
        setShowInputField(false);
        setListName(""); 
      } catch (error) {
        console.error("Error creating board:", error);
     
      }
    }
  };

  const handleClose = () => {
    setShowInputField(false);
    setListName(""); 
  };

  return (
    <div>
      <Dialog open={showInputField} onClose={handleClose} maxWidth="xs" fullWidth>
        <DialogTitle>Create New List</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="outlined-basic"
            label="List Name"
            variant="outlined"
            fullWidth
            value={listName}
            onChange={handleListNameChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleCreateList} color="primary">
            Create
          </Button>
        </DialogActions>
      </Dialog>

      {!showInputField && (
        <Card style={{backgroundColor: "#BFDBFE" ,width:"500px", padding:"0px 10px", float:"right"}}>
        <CardContent>
        <Button variant="outlined" style={{width:"250px", border:"none", color:"black" ,textAlign:"center"}}onClick={handleAddListClick} >
          Add List
        </Button>
        </CardContent>
        </Card>
      )}
    </div>
  );
 


};

export default CreateList;
