
import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { Card, CardContent, Dialog, DialogTitle, DialogContent, DialogActions } from "@mui/material";
import axios from "axios";

const key = "f1fc58945d17dbaac59bedb8364274cf";
const token = "ATTA6efd8ca5a96337966c66ba73c0c2a54b4a1c42008d236f8e9fff33bfdcd8034638704FC5";

const CreateCard = (props) => {
    const {id,updateCards}=props
 
  const [cardName, setCardName] = useState("");
  const [showInputField, setShowInputField] = useState(false);

  const handleCardNameChange = (e) => {
    setCardName(e.target.value);
  };

  const handleAddCardClick = () => {
    setShowInputField(true);
  };

  const handleCreateCard = async () => {
    const API_POST_URL = `https://api.trello.com/1/cards?idList=${id}&name=${cardName}&key=${key}&token=${token}`;
    if (cardName.trim()) {
      try {
        const response = await axios.post(API_POST_URL);
       
        updateCards(response.data);
        setShowInputField(false);
        setCardName(""); 
      } catch (error) {
        console.error("Error creating board:", error);
     
      }
    }
  };

  const handleClose = () => {
    setShowInputField(false);
    setCardName(""); 
  };

  return (
    <div>
      <Dialog open={showInputField} onClose={handleClose} maxWidth="xs" fullWidth>
        <DialogTitle>Create New Card</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="outlined-basic"
            label="Card Name"
            variant="outlined"
            fullWidth
            value={cardName}
            onChange={handleCardNameChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleCreateCard} color="primary">
            Create
          </Button>
        </DialogActions>
      </Dialog>

      {!showInputField && (
        <Card style={{width:"350px",alignContent:"center" ,margin:"10px 0px 0px 0px"}}>
        <CardContent>
        <Button variant="outlined" style={{width:"250px", border:"none", color:"black", }}onClick={handleAddCardClick} >
          Add Card
        </Button>
        </CardContent>
        </Card>
      )}
    </div>
  );
};

export default CreateCard;

