import React from "react";

const key = "f1fc58945d17dbaac59bedb8364274cf";
const token =
  "ATTA6efd8ca5a96337966c66ba73c0c2a54b4a1c42008d236f8e9fff33bfdcd8034638704FC5";

import { useState, useEffect } from "react";
import axios from "axios";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

import { useNavigate } from "react-router-dom";
import CreateCard from "./CreateCard";
import DeleteIcon from '@mui/icons-material/Delete';
import AllCheckLists from "../CheckList/AllCheckLists";

const AllCards = ({ id }) => {
  const [cards, setCards] = useState(null);
  const [selectedCard, setSelectedCard] = useState(null);
 
  console.log(selectedCard)
  const updateCards = (newData) => {
    setCards([...cards, newData]);
  };


  useEffect(() => {
    const url = `https://api.trello.com/1/lists/${id}/cards?key=${key}&token=${token}`;

    axios
      .get(url)
      .then((response) => {
        setCards(response.data);
      })
      .catch((error) => {
        console.error("Error fetching boards", error);
      });
  }, [id]);

  const handleselectedcard= (cardid) => {
    if (cardid === selectedCard) {
      
      setSelectedCard(null);
    } else {
      setSelectedCard(cardid);
    } 
  }

const deleteEachCard=(cardid)=>{
   const url=`https://api.trello.com/1/cards/${cardid}?key=${key}&token=${token}`
   axios.delete(url)
   .then(()=>{
    handleDeleteCard(cardid);
   })
   .catch((error)=>{
    console.error('Error deleting cards',error);
   })
}

  const handleDeleteCard=(cardid)=>{
    setCards(cards.filter(card=>card.id!==cardid))
    if (selectedCard === cardid) {
      setSelectedCard(null); 
    }
  }

 

  return (
    <div>
      {cards ? (
        <div className="flex flex-col">
          {cards.map((card) => (
            <Card
              key={card.id}
              style={{ backgroundColor: "white", margin: "10px 0px 0px 0px" }}
              onClick={()=>handleselectedcard(card.id)}
              
            >
              <CardContent>
                <Typography>{card.name}
                </Typography>
                <DeleteIcon style={{ cursor: 'pointer' ,float:"right" ,marginTop:"-20px"}} onClick={(e) =>{e.stopPropagation();
                   deleteEachCard(card.id)
                   }}
                   />
              </CardContent>
            </Card>
           
            
          ))}
        </div>
      ) : (
        <p>Loading...</p>
      )}
       {selectedCard!==null && <><AllCheckLists id={selectedCard}/> </>}
       
      <CreateCard id={id} updateCards={updateCards} />
    </div>
  );
};

export default AllCards;
