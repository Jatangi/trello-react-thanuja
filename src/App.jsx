import { useState } from "react";

import { BrowserRouter, Route, Routes } from "react-router-dom";

import { Header } from "./Components/Header";

import Allboards from "./Components/Boards/Allboards";

import CreateList from "./Components/Lists/CreateList";
import AllLists from "./Components/Lists/AllLists";


export function App() {
  return (
    <BrowserRouter>
    <>
        <Header/>
        {/* <Homepage/> */}
      
    </>
    <Routes>
      <Route path="/" element={<Allboards/>}></Route>
      <Route path="/boards/:id" element={<AllLists />}></Route>
    </Routes>
    </BrowserRouter>


  );
}


